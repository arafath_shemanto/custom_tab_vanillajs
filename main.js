const btns = document.querySelectorAll(".tab_button");
const wrapper = document.querySelector(".wrapper");
const tab_content_box = document.querySelectorAll(".tab_content_box");

wrapper.addEventListener("click", function (e) {
  let id = e.target.dataset.id;
  if (id) {
    btns.forEach(function (btn) {
      btn.classList.remove("active");
      e.target.classList.add("active");
    });
    tab_content_box.forEach(function (content) {
      content.classList.remove("active");
    });
    let elemnt = document.getElementById(id);
    elemnt.classList.add("active");
  }
});
